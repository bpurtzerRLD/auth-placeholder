This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

to run outside of docker, you will need node installed.

Install dependencies

```bash
npm install
```

First, run the development server:

```bash
npm run dev
```

Open [http://localhost:8080](http://localhost:8080) with your browser to see the result.

You can start editing the page by modifying `pages/index.js`. The page auto-updates as you edit the file.

## Docker
Build your docker image
```bash
docker build . -t auth
```

Start up docker
```bash
docker-compose up
```