const jwt = require('jsonwebtoken')
const jwt_secret = 'shhhThisIsSecret'

export default async function handler(req, res) {
    const authTokenString = req.headers['x-auth-token'] || req.headers['X-AUTH-TOKEN'] || req.headers['x_auth_token']
    try {
      const authToken = await jwt.verify(authTokenString, jwt_secret)
      const jwt_header = jwt.sign({
        roles: ['user', 'admin'],
        ...authToken
      }, jwt_secret)
      console.log('SUCCESSFUL AUTH REQUEST')
      return res.setHeader('X-Forwarded-User', jwt_header).status(200).json({})
    } catch (e) {
      console.log('AUTH REQUEST ERROR', e)
      res.status(403).json({})
    }
  }